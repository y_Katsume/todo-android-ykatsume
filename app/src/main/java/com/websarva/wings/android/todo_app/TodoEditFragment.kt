package com.websarva.wings.android.todo_app

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_todo_edit.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class TodoEditFragment : Fragment() {

    private val args: TodoEditFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_todo_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val todo = args.todo
        if (todo != null) {
            setUpTexts(todo)
        }
        setUpCompletionButton(todo?.id)
        editTextChangeListener()
        text_picker_todo_date.setOnClickListener {
            showDatePickerDialog()
        }
    }

    private fun editTextChangeListener() {
        edit_todo_title.addTextChangedListener(object : CustomTextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                text_title_counter.text = s?.length.toString()
                changeTitleCounterTextColor()
                changeCompletionButtonEnabled()
            }
        })

        edit_todo_detail.addTextChangedListener(object : CustomTextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                text_detail_counter.text = s?.length.toString()
                changeDetailCounterTextColor()
                changeCompletionButtonEnabled()
            }
        })
    }

    private interface CustomTextWatcher : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun afterTextChanged(s: Editable?) {}
    }

    private fun changeTitleCounterTextColor() {
        val color = if (edit_todo_title.length() <= MAX_TITLE_LIMIT) Color.GRAY else Color.RED
        text_title_counter.setTextColor(color)
    }

    private fun changeDetailCounterTextColor() {
        val color = if (edit_todo_detail.length() <= MAX_DETAIL_LIMIT) Color.GRAY else Color.RED
        text_detail_counter.setTextColor(color)
    }

    private fun changeCompletionButtonEnabled() {
        val isTitleLimitOver = edit_todo_title.length() in 1..MAX_TITLE_LIMIT
        val isDetailLimitOver = edit_todo_detail.length() > MAX_DETAIL_LIMIT
        button_register.isEnabled = isTitleLimitOver && !isDetailLimitOver
    }

    private fun showDatePickerDialog() {
        val calender = Calendar.getInstance()
        val year = calender.get(Calendar.YEAR)
        val month = calender.get(Calendar.MONTH)
        val day = calender.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            requireContext(), DatePickerDialog.OnDateSetListener { _, y, m, d ->
                text_picker_todo_date.text = ("$y/${m + 1}/$d")
            }
            , year, month, day
        )
        datePickerDialog.setButton(
            DialogInterface.BUTTON_NEUTRAL,
            getString(R.string.button_picker_date_delete)
        ) { _, _ ->
            text_picker_todo_date.text = ""
        }
        datePickerDialog.show()
    }

    private suspend fun createTodo() {
        try {
            val response = TodoApiClient().apiRequest.createTodo(makeBody()).execute()
            if (response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    onSuccess(isCreateTodo = true)
                }
            } else {
                val body =
                    Gson().fromJson(response.errorBody()!!.string(), BaseResponse::class.java)
                withContext(Dispatchers.Main) {
                    showErrorDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorDialog(getString(R.string.unexpected_error_message))
            }
        }
    }

    private suspend fun updateTodo(id: Int, todoRequestBody: TodoRequestBody) = try {
        val response = TodoApiClient().apiRequest.updateTodo(id, todoRequestBody).execute()
        if (response.isSuccessful) {
            withContext(Dispatchers.Main) {
                onSuccess(isCreateTodo = false)
            }
        } else {
            val body =
                Gson().fromJson(response.errorBody()!!.string(), BaseResponse::class.java)
            withContext(Dispatchers.Main) {
                showErrorDialog(body.errorMessage)
            }
        }
    } catch (e: Exception) {
        withContext(Dispatchers.Main) {
            showErrorDialog(getString(R.string.unexpected_error_message))
        }
    }

    private fun setUpTexts(todo: Todo) {
        edit_todo_title.setText(todo.title)
        todo.detail?.let {
            edit_todo_detail.setText(todo.detail)
        }
        todo.date?.let {
            val format = SimpleDateFormat(DATE_PATTERN, Locale.getDefault())
            text_picker_todo_date.text = format.format(todo.date)
        }
    }

    private fun setUpCompletionButton(id: Int?) {
        button_register.setOnClickListener {
            CoroutineScope(Dispatchers.Default).launch {
                id?.let { updateTodo(it, makeBody()) } ?: createTodo()
            }
        }
        button_register.text =
            getString(if (id != null) R.string.update_button else R.string.register_button)
        changeCompletionButtonEnabled()
    }

    private fun makeBody(): TodoRequestBody {
        val title = edit_todo_title.text.toString()
        val detail = edit_todo_detail?.text?.toString()
        val format = SimpleDateFormat(DATE_PATTERN, Locale.getDefault())
        val dateText = text_picker_todo_date.text
        val date = if (dateText.isBlank()) {
            ""
        } else {
            val parseDate = format.parse(dateText.toString())
            SimpleDateFormat(FORMAT_DATE_FOR_SERVER, Locale.getDefault()).format(parseDate)
        }
        return TodoRequestBody(title, detail, date)
    }

    private fun onSuccess(isCreateTodo: Boolean) {
        val message =
            getString(if (isCreateTodo) R.string.todo_registered_message else R.string.todo_updated_message)
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.value =
            Event(message)
        findNavController().popBackStack()
    }

    companion object {
        private const val MAX_TITLE_LIMIT = 100
        private const val MAX_DETAIL_LIMIT = 1000
        private const val DATE_PATTERN = "yyyy/M/d"
        private const val FORMAT_DATE_FOR_SERVER = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
}
