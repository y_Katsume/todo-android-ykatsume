package com.websarva.wings.android.todo_app

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TodoListFragment : Fragment() {
    private lateinit var adapter: RecyclerAdapter
    private var isDeleteMode = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.observeEvent(this) {
            Snackbar.make(requireView(), it, Snackbar.LENGTH_LONG).show()
        }
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val decorator = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)

        recycler_todo.also {
            it.layoutManager = LinearLayoutManager(activity)
            it.addItemDecoration(decorator)
            adapter = RecyclerAdapter(::onClickRow)
            it.adapter = adapter
        }
        CoroutineScope(Default).launch {
            fetchTodoList()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_todo_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.create_icon -> goToEditFragment()
            R.id.delete_icon -> changeDeleteMode(item)
        }
        return super.onOptionsItemSelected(item)
    }

    private suspend fun fetchTodoList() {
        try {
            val response = TodoApiClient().apiRequest.getTodos().execute()
            if (response.isSuccessful) {
                withContext(Main) {
                    adapter.todoList = response.body()?.todos!!
                }
            } else {
                val body =
                    Gson().fromJson(response.errorBody()!!.string(), BaseResponse::class.java)
                withContext(Main) {
                    showErrorDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorDialog(getString(R.string.unexpected_error_message))
            }
        }
    }

    private suspend fun deleteTodo(id: Int) {
        try {
            val response = TodoApiClient().apiRequest.deleteTodo(id).execute()
            if (response.isSuccessful) {
                fetchTodoList()
            } else {
                val body =
                    Gson().fromJson(response.errorBody()!!.string(), BaseResponse::class.java)
                withContext(Main) {
                    showErrorDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorDialog(getString(R.string.unexpected_error_message))
            }
        }
    }

    private fun onClickRow(todo: Todo) {
        if (isDeleteMode) {
            showCheckDeleteDialog(todo)
        } else {
            goToEditFragment(todo)
        }
    }

    private fun goToEditFragment(todo: Todo? = null) {
        val action = TodoListFragmentDirections.actionEdit(todo)
        findNavController().navigate(action)
    }

    private fun changeDeleteMode(item: MenuItem) {
        isDeleteMode = !isDeleteMode
        val resourceDrawable =
            if (isDeleteMode) R.drawable.ic_done else R.drawable.ic_delete
        item.setIcon(resourceDrawable)
    }

    private fun showCheckDeleteDialog(todo: Todo) {
        AlertDialog.Builder(context)
            .setMessage(getString(R.string.todo_delete_message, todo.title))
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                CoroutineScope(Default).launch {
                    deleteTodo(todo.id)
                }
            }
            .show()
    }
}
